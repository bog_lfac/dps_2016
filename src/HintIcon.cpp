/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <HintIcon.hpp>

HintIcon::HintIcon(Scene* scene
		   , std::string img_path
		   , sf::FloatRect area)
  : Widget(scene, area)
  
{
  std::string path = (std::string(DATA_PATH)
		      + "/images/"
		      + img_path);
  
  m_texture.loadFromFile( path );
  
  m_background.setTexture(&m_texture);
  
  m_background.setOutlineColor(sf::Color::Black);
}

/*virtual*/ void HintIcon::update()
{
  Widget::update();
}

/*virtual*/ void HintIcon::display()
{
  Widget::display();
}


HintIcon::~HintIcon()
{
  
}
