/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <GameScene.hpp>
#include <Grid.hpp>
#include <HintIconFactory.hpp>

GameScene::GameScene(Core* core)
  : Scene(core)
{

  m_grid = std::make_unique<Grid>( window() );
  
  {
    HintIconFactory factory(this);
    sf::FloatRect fr (128, 128, 256, 256);
    m_dialog_player = std::make_unique<LineArea>(this, "Replique hello world !", factory.createImitate( sf::FloatRect(0, 0, 32, 32) ), fr);
  }

}

/*virtual*/ void GameScene::update()
{
  m_dialog_player->update();  
}

/*virtual*/ void GameScene::display()
{
  m_dialog_player->display();
}

GameScene::~GameScene()
{

}
