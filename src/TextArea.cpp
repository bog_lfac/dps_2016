/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <TextArea.hpp>

TextArea::TextArea(Scene* scene, sf::FloatRect area)
  : Widget (scene, area)
  , m_MIN_CHAR_SIZE (1)
  , m_MAX_CHAR_SIZE (50)
{
  m_font.loadFromFile(DATA_PATH "/fonts/LiberationSans-Regular.ttf");

  m_text.setFont(m_font);
  m_text.setFillColor(sf::Color::Black);
  m_text.setCharacterSize(m_MAX_CHAR_SIZE);

  m_background.setFillColor(sf::Color(150, 150, 150));
}

std::string TextArea::computeFormattedString()
{
  sf::Text text;
  std::string str = "";
  float remaining_width = m_area.width;
  float remaining_height = m_area.height;
  float max_height = 0;
  float line_spacing;
  bool must_recompute = false;
  line_spacing = m_font.getLineSpacing( m_text.getCharacterSize() );

  text.setFont(m_font);
  text.setString(str);
  text.setPosition({m_area.left, m_area.width});
  text.setCharacterSize( m_text.getCharacterSize() );

  size_t i = 0;

  while( i < m_string.size() && remaining_height > 0 )
    {
      std::string word = "";

      // get next word
      while( m_string[i] != ' '
	     && i < m_string.size() )
	{
	  word += m_string[i];
	  i++;
	}

      text.setString(word + " ");
      float word_space_width = text.getLocalBounds().width;

      // get max char height
      if(text.getGlobalBounds().height > max_height)
	{
	  max_height = text.getGlobalBounds().height;
	}

      // make sure the word fit in a line
      if(text.getGlobalBounds().width > m_area.width)
	{
	  must_recompute = true;
	  break;
	}

      // the word can be added to the current line
      if(remaining_width - word_space_width >= 0 )
	{
	  remaining_width -= word_space_width;
	  str += word + " ";
	}
      // no space remaining for this word,
      // -> next line
      else
      	{
      	  str += "\n" + word + " ";

      	  remaining_width = ( m_area.width
			      - word_space_width);

	  remaining_height -= ( max_height
				+ line_spacing );
	  max_height = 0;
      	}

      i++;
    }

  // we haven't achieve to fit all the text into the area
  // or we have to recompute the area because of a too long
  // word.
  if(remaining_height < 0 || must_recompute)
    {
      if(m_text.getCharacterSize() - 1 < m_MIN_CHAR_SIZE)
	{
	  return str;
	}

      m_text.setCharacterSize(m_text.getCharacterSize() - 1);

      // -> recompte the string with a lower character size
      return computeFormattedString();
    }

  return str;
}

/*virtual*/ void TextArea::update()
{
  Widget::update();
  m_text.setPosition({m_area.left, m_area.top});
}

/*virtual*/ void TextArea::display()
{
  Widget::display();
  window()->draw(m_background);
  window()->draw(m_text);
}

TextArea::~TextArea()
{

}
