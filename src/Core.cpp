/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Core.hpp>
#include <GameScene.hpp>

Core::Core(sf::RenderWindow* win)
  : m_window(win)
  , m_is_running (false)
  , m_current_scene (nullptr)
{
  m_window->setFramerateLimit(60.0);
  m_game_scene = std::make_unique<GameScene>(this);
  m_current_scene = m_game_scene.get();
}

void Core::update()
{
  if(m_current_scene)
    {
      m_current_scene->update();
    }
}

void Core::display()
{
  if(m_current_scene)
    {
      m_current_scene->display();
    }
}


void Core::run()
{
  m_is_running = true;
  sf::Event event;
  
  while( m_is_running )
    {
      while( m_window->pollEvent(event) )
	{
	  if( event.type == sf::Event::Closed
	      || ( event.type == sf::Event::KeyPressed
		   && event.key.code == sf::Keyboard::Escape ) )
	    {
	      m_is_running = false;
	    }
	}
      
      update();

      m_window->clear(sf::Color::White);
      display();
      m_window->display();
    }

  m_window->close();
}


Core::~Core()
{
  
}
