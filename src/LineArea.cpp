/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LineArea.hpp>
#include <HintIcon.hpp>
#include <TextArea.hpp>

LineArea::LineArea(Scene* scene
		    , std::string text
		    , HintIcon* icon
		    , sf::FloatRect area)
  : Entity (scene)
  , m_area (area)
  , m_icon ( std::unique_ptr<HintIcon>(icon) )
{
  {
    sf::FloatRect new_icon_size = m_icon->area();
    new_icon_size.left += m_area.left;
    new_icon_size.top += m_area.top;
    m_icon->resize(new_icon_size);
  }

  m_text = std::make_unique<TextArea>(scene, m_area);
  m_text->string(text);
}

/*virtual*/ void LineArea::update()
{
  m_text->update();
  m_icon->update();
}

/*virtual*/ void LineArea::display()
{
  m_text->display();
  m_icon->display();
}

LineArea::~LineArea()
{

}
