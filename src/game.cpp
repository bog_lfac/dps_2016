#include <iostream>
#include <Core.hpp>
#include <SFML/Graphics.hpp>

int main()
{
  sf::VideoMode mode = sf::VideoMode::getDesktopMode();
  mode.width *= 0.8;
  mode.height *= 0.8;
  
  sf::RenderWindow window(mode
			  , "DPS -v1"
			  , sf::Style::Titlebar
			  | sf::Style::Close);
  
  Core core(&window);
  
  core.run();
  
  return EXIT_SUCCESS;
}
