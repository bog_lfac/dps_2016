/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GAMESCENE_HPP
#define GAMESCENE_HPP
#include <iostream>
#include <memory>
#include <Scene.hpp>
#include <LineArea.hpp>

class Grid;

class GameScene : public Scene
{
 public:
  explicit GameScene(Core* core);
  virtual ~GameScene();

  virtual void update();
  virtual void display();

 protected:
  std::unique_ptr<Grid> m_grid;
  std::unique_ptr<LineArea> m_dialog_player;

 private:
  GameScene( GameScene const& gamescene ) = delete;
  GameScene& operator=( GameScene const& gamescene ) = delete;
};

#endif
