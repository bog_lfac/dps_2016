/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ENTITY_HPP
#define ENTITY_HPP
#include <iostream>
#include <Scene.hpp>

class Entity
{
 public:
  explicit Entity(Scene* scene)
    : m_scene (scene)
  {
  }
  
  virtual ~Entity() {}

  virtual inline Scene* scene() { return m_scene; }

  virtual inline sf::RenderWindow* window()
  {
    return m_scene->window();
  }

  virtual void update() = 0;
  virtual void display() = 0;
  
 protected:
  Scene* m_scene;
  
 private:
  Entity( Entity const& entity ) = delete;
  Entity& operator=( Entity const& entity ) = delete;
};

#endif
