/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef HINTICONFACTORY_HPP
#define HINTICONFACTORY_HPP
#include <iostream>
#include <HintIcon.hpp>
#include <Scene.hpp>
#include <SFML/Graphics.hpp>

class HintIconFactory
{
 public:
  explicit HintIconFactory(Scene* scene)
    : m_scene (scene)
  {
  }
  
  virtual ~HintIconFactory()
    {
    }

  HintIcon* createImitate(sf::FloatRect area)
  {
    return new HintIcon(m_scene
    			, "imitate.png"
    			, area);
  }
  
 protected:
  Scene* m_scene;
  
 private:
  HintIconFactory( HintIconFactory const& hinticonfactory ) = delete;
  HintIconFactory& operator=( HintIconFactory const& hinticonfactory ) = delete;
};

#endif
