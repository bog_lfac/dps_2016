/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef HINTICON_HPP
#define HINTICON_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include <Widget.hpp>

class HintIcon : public Widget
{
 public:
  explicit HintIcon(Scene* scene
		    , std::string img_path
		    , sf::FloatRect area);
  
  virtual ~HintIcon();

  virtual void update();
  virtual void display();
    
 protected:
  sf::Texture m_texture;
  
 private:
  HintIcon( HintIcon const& hinticon ) = delete;
  HintIcon& operator=( HintIcon const& hinticon ) = delete;
};

#endif
