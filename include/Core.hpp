/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#define DATA_PATH "../assets"

class Scene;
class GameScene;

class Core
{
 public:
  explicit Core(sf::RenderWindow* win);
  virtual ~Core();

  void update();
  void display();

  void run();

  inline sf::RenderWindow*  window() { return m_window; }
  
 protected:
  sf::RenderWindow* m_window;
  bool m_is_running;
  Scene* m_current_scene;
  std::unique_ptr<GameScene> m_game_scene;
  
 private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
};

#endif
