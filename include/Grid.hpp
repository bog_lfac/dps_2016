/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRID_HPP
#define GRID_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

class Grid
{
 public:
  explicit Grid(sf::RenderWindow* window)
    : m_window(window)
  {
    
  }
  
  virtual ~Grid() {}

  float x(size_t x)
  {
    return static_cast<float>(x) * m_window->getSize().x/12.0;
  }

  float y(size_t y)
  {
    return static_cast<float>(y) * m_window->getSize().y/12.0;
  }

 protected:
  sf::RenderWindow* m_window;
  
 private:
  Grid( Grid const& grid ) = delete;
  Grid& operator=( Grid const& grid ) = delete;
};

#endif
