/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LINEAREA_HPP
#define LINEAREA_HPP
#include <iostream>
#include <Entity.hpp>
#include <SFML/Graphics.hpp>

class TextArea;
class HintIcon;

class LineArea : public Entity
{
 public:
  explicit LineArea(Scene* scene
		    , std::string text
		    , HintIcon* icon
		    , sf::FloatRect area);
  
  virtual ~LineArea();

  virtual void update() override;
  virtual void display() override;
  
 protected:
  sf::FloatRect m_area;
  std::unique_ptr<HintIcon> m_icon;
  std::unique_ptr<TextArea> m_text;
  sf::RectangleShape m_rect;
  
 private:
  LineArea( LineArea const& linearea ) = delete;
  LineArea& operator=( LineArea const& linearea ) = delete;
};

#endif
