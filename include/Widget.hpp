/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef WIDGET_HPP
#define WIDGET_HPP
#include <iostream>
#include <Entity.hpp>
#include <SFML/Graphics.hpp>

class Widget : public Entity
{
 public:
  explicit Widget(Scene* scene, sf::FloatRect area);
  virtual ~Widget();

  virtual void update();
  virtual void display();

  inline void resize(sf::FloatRect area) { m_area = area; }
  inline sf::FloatRect area() const { return m_area; }

  inline sf::RectangleShape& background() { return m_background; }
  
 protected:
  sf::FloatRect m_area;
  sf::RectangleShape m_background;
  float m_thickness;
  
 private:
  Widget( Widget const& widget ) = delete;
  Widget& operator=( Widget const& widget ) = delete;
};

#endif
