/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef TEXTAREA_HPP
#define TEXTAREA_HPP
#include <iostream>
#include <Widget.hpp>

class TextArea : public Widget
{
 public:
  explicit TextArea(Scene* scene, sf::FloatRect area);
  virtual ~TextArea();

  inline void string(std::string str)
  {
    m_text.setCharacterSize(m_MAX_CHAR_SIZE);
    m_string = str;
    m_formatted_string = computeFormattedString();
    m_text.setString(m_formatted_string);
  }

  virtual void update() override;
  virtual void display() override;

 protected:
  std::string m_string;
  std::string m_formatted_string;
  sf::Font m_font;
  sf::Text m_text;

  float const m_MIN_CHAR_SIZE;
  float const m_MAX_CHAR_SIZE;

  std::string computeFormattedString();

 private:
  TextArea( TextArea const& textarea ) = delete;
  TextArea& operator=( TextArea const& textarea ) = delete;
};

#endif
