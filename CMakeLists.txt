cmake_minimum_required(VERSION 3.0)

project(dps)

file(GLOB
  sources
  src/*.cpp
  )

include_directories("include")

add_definitions("-Wall -Wextra -std=c++14 -g")

add_executable(game.elf
  ${sources})

find_package(PkgConfig)
pkg_check_modules(SFML REQUIRED sfml-graphics sfml-audio)

target_link_libraries(game.elf
  ${SFML_LDFLAGS})